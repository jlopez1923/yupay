import React, { useCallback, useState } from 'react'
import NavBarAdmin from './NavBarAdmin'
import Main from './Main'
import Contents from './Contents/index.jsx'
import Questionnaires from './Questionnaires/index.jsx'
import Courses from './Courses/index.jsx'

import { ContentProvider } from './contexts/contents'
import { QuestionnairesProvider } from './contexts/questionnaires'

import './App.css'

const App = () => {
  //Fix the Main component like first interface
  const [panel, setPanel] = useState('Main')

  //Handlers that switch the panel layout
  const handleDisplayMain = useCallback(() => {
    setPanel('Main')
  }, [panel, setPanel])

  const handleDisplayContents = useCallback(() => {
    setPanel('Contents')
  }, [panel, setPanel])

  const handleDisplayQuestionnaires = useCallback(() => {
    setPanel('Questionnaires')
  }, [panel, setPanel])

  const handleDisplayCourses = useCallback(() => {
    setPanel('Courses')
  }, [panel, setPanel])

  return (
    <div className="App">
      <ContentProvider>
        <QuestionnairesProvider>
          <NavBarAdmin
            handleDisplayMain={handleDisplayMain}
            handleDisplayContents={handleDisplayContents}
            handleDisplayQuestionnaires={handleDisplayQuestionnaires}
            handleDisplayCourses={handleDisplayCourses}
          />
          <Main show={panel === 'Main'} />
          <Contents show={panel === 'Contents'} />
          <Questionnaires show={panel === 'Questionnaires'} />
          <Courses show={panel === 'Courses'} />
        </QuestionnairesProvider>
      </ContentProvider>
    </div>
  )
}

export default App
