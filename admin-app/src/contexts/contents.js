/**To better understand the structure of this project
 * study this article:
 * https://kentcdodds.com/blog/application-state-management-with-react
 */
import React from 'react'
import uuidV4 from 'uuid/v4'
import cloneDeep from 'lodash/cloneDeep'
import get from 'lodash/get'

/* This block of sentences is necesary for to create
 folders and files in electron since admin-app*/
const electronRequire = window.require
const electron = electronRequire('electron')
const fs = electronRequire('fs')
const pathModule = electronRequire('path')
const shell = electronRequire('shelljs')
const app = electron.remote.app

const userPath = app.getPath('userData')
const yupayPath = pathModule.join(userPath, 'yupay-files')

// Help to create files in Linux
shell.mkdir('-p', yupayPath)

const contentsFile = pathModule.join(yupayPath, 'yupay-contents.json')
const contentsFolder = pathModule.join(yupayPath, 'yupay-contents')

if (!fs.existsSync(contentsFolder)) {
  fs.mkdirSync(contentsFolder)
}
//Create the content context
const ContentContext = React.createContext()

function getExtension(name) {
  const parts = name.split('.')
  return parts[parts.length - 1]
}
//Hook to create the context of Contents component
export function useContent() {
  const context = React.useContext(ContentContext)
  if (!context) {
    throw new Error(`useCount must be used within a ContentProvider`)
  }
  return context
}

export function ContentProvider(props) {
  const [content, _setContent] = React.useState({ topics: [] })
  const [resources, setResources] = React.useState([])

  React.useEffect(() => {
    const initialData = fs.existsSync(contentsFile)
      ? JSON.parse(fs.readFileSync(contentsFile, 'utf-8'))
      : { topics: [] }

    setContent(initialData)
  }, [])

  const removeResourceFiles = resource => {
    if (resource.img) {
      const imgFile = pathModule.join(
        contentsFolder,
        `${resource.id}.${resource.img}`
      )
      fs.unlinkSync(imgFile)
    }
    if (resource.src) {
      const contentFile = pathModule.join(
        contentsFolder,
        `${resource.id}.${resource.src}`
      )
      fs.unlinkSync(contentFile)
    }
  }

  const setContent = React.useCallback(
    newData => {
      fs.writeFile(contentsFile, JSON.stringify(newData), 'utf-8', () => {})
      _setContent(newData)
      const resourceArr = []
      for (const topic of newData.topics) {
        for (const subtopic of topic.subtopics) {
          for (const resource of subtopic.resources) {
            resourceArr.push(resource)
          }
        }
      }
      setResources(resourceArr)
    },
    [_setContent]
  )

  const model = React.useMemo(
    () => ({
      new(path) {
        const name = path[path.length - 1]
        const clonedData = cloneDeep(content)
        const targetArr = get(clonedData, path)
        targetArr.push({
          id: uuidV4(),
          title: 'Nuevo Item',
          ...(name === 'resources'
            ? {
                author: '',
                type: '',
                intelligence: '',
                learningStyle: '',
                img: '',
                src: '',
              }
            : name === 'topics'
            ? { subtopics: [] }
            : { resources: [] }), // subtopic
        })
        setContent(clonedData)
      },
      remove(path, idx, openItem, setOpenItem) {
        const clonedData = cloneDeep(content)
        const targetArr = get(clonedData, path)
        const lastPart = path[path.length - 1]
        const obj = targetArr[idx]
        if (lastPart === 'topics') {
          for (const subtopic of obj.subtopics) {
            for (const resource of subtopic.resources) {
              removeResourceFiles(resource)
            }
          }
        } else if (lastPart === 'subtopics') {
          for (const resource of obj.resources) {
            removeResourceFiles(resource)
          }
        } else if (lastPart === 'resources') {
          removeResourceFiles(obj)
        }
        targetArr.splice(idx, 1)
        setContent(clonedData)
        if (openItem === idx) {
          setOpenItem(-1)
        }
      },
      editTitle(path, idx, newTitle) {
        const clonedData = cloneDeep(content)
        const targetArr = get(clonedData, path)
        targetArr[idx].title = newTitle ? newTitle : 'Escribe un título'
        setContent(clonedData)
      },
      down(path, idx) {
        if (idx < content.topics.length - 1) {
          const clonedData = cloneDeep(content)
          const targetArr = get(clonedData, path)
          const aux = targetArr[idx]
          targetArr[idx] = targetArr[idx + 1]
          targetArr[idx + 1] = aux
          setContent(clonedData)
        }
      },
      up(path, idx) {
        if (idx > 0) {
          const clonedData = cloneDeep(content)
          const targetArr = get(clonedData, path)
          const aux = targetArr[idx]
          targetArr[idx] = targetArr[idx - 1]
          targetArr[idx - 1] = aux
          setContent(clonedData)
        }
      },
      editResource(resourcePath, formData) {
        const clonedData = cloneDeep(content)
        const resource = get(clonedData, resourcePath)
        const id = resource.id || uuidV4()
        resource.id = id
        resource.author = formData.author
        resource.type = formData.type
        resource.intelligence = formData.intelligence
        resource.learningStyle = formData.learningStyle
        // if the user choose an image
        if (typeof formData.img === 'object') {
          // if there is already a file, remove it
          if (resource.img) {
            fs.unlinkSync(
              pathModule.join(contentsFolder, `${id}.${resource.img}`)
            )
          }
          const imgExt = getExtension(formData.img.name)
          const imgPath = pathModule.join(contentsFolder, `${id}.${imgExt}`)
          fs.writeFileSync(imgPath, '', 'utf-8')
          fs.createReadStream(formData.img.path).pipe(
            fs.createWriteStream(imgPath)
          )
          resource.img = imgExt
        }
        // if the user choose a source
        if (typeof formData.src === 'object') {
          // if there is already a file, remove it
          if (resource.src) {
            fs.unlinkSync(
              pathModule.join(contentsFolder, `${id}.${resource.src}`)
            )
          }
          const srcExt = getExtension(formData.src.name)
          const srcPath = pathModule.join(contentsFolder, `${id}.${srcExt}`)
          fs.writeFileSync(srcPath, '', 'utf-8')
          fs.createReadStream(formData.src.path).pipe(
            fs.createWriteStream(srcPath)
          )
          resource.type = srcExt === 'pdf' ? 'pdf' : 'video'
          resource.src = srcExt
        }
        setContent(clonedData)
      },
    }),
    [content, setContent]
  )

  const value = React.useMemo(
    () => ({ content, setContent, resources, model }),
    [content, setContent, resources, model]
  )

  return <ContentContext.Provider value={value} {...props} />
}
