import React from 'react'
import uuidV4 from 'uuid/v4'
import cloneDeep from 'lodash/cloneDeep'
import get from 'lodash/get'

const electronRequire = window.require
const electron = electronRequire('electron')
const fs = electronRequire('fs')
const pathModule = electronRequire('path')
const shell = electronRequire('shelljs')
const { ipcRenderer } = electronRequire('electron')
const app = electron.remote.app

const userPath = app.getPath('userData')
const yupayPath = pathModule.join(userPath, 'yupay-files')

shell.mkdir('-p', yupayPath)

const questionnairesFile = pathModule.join(
  yupayPath,
  'yupay-questionnaires.json'
)
const questionnairesFolder = pathModule.join(yupayPath, 'yupay-questionnaires')

if (!fs.existsSync(questionnairesFolder)) {
  fs.mkdirSync(questionnairesFolder)
}

const QuestionnariesContext = React.createContext()

function getExtension(name) {
  const parts = name.split('.')
  return parts[parts.length - 1]
}

function removeQuestionFile(question, fieldName) {
  if (question[fieldName]) {
    const imgFile = pathModule.join(
      questionnairesFolder,
      `${question.id}.${question[fieldName]}`
    )
    fs.unlinkSync(imgFile)
  }
}

export function useQuestionnaires() {
  const context = React.useContext(QuestionnariesContext)
  if (!context) {
    throw new Error(`useCount must be used within a QuestionnairesProvider`)
  }
  return context
}

export function QuestionnairesProvider(props) {
  const [data, _setData] = React.useState({ questionnaires: [] })

  React.useEffect(() => {
    const initialData = fs.existsSync(questionnairesFile)
      ? JSON.parse(fs.readFileSync(questionnairesFile, 'utf-8'))
      : { questionnaires: [] }
    setData(initialData)
  }, [])

  const removeQuestionFiles = question => {
    removeQuestionFile(question, 'img')
    removeQuestionFile(question, 'answer0')
    removeQuestionFile(question, 'answer1')
    removeQuestionFile(question, 'answer2')
    removeQuestionFile(question, 'answer3')
  }

  const setData = React.useCallback(
    newData => {
      fs.writeFile(questionnairesFile, JSON.stringify(newData), 'utf-8', () => {
        ipcRenderer.send('refresh-questionnaires')
      })
      _setData(newData)
    },
    [_setData]
  )

  const model = React.useMemo(
    () => ({
      new(path) {
        const name = path[path.length - 1]
        const clonedData = cloneDeep(data)
        const targetArr = get(clonedData, path)
        targetArr.push({
          id: uuidV4(),
          title: 'Nuevo Item',
          ...(name === 'questions'
            ? {
                img: '',
                rightIndex: 0,
                answer0: '',
                answer1: '',
                answer2: '',
                answer3: '',
                responseTime: 60,
                relatedResource: '',
              }
            : {
                // questionnaires
                questions: [],
              }),
        })
        setData(clonedData)
      },
      remove(path, idx, openItem, setOpenItem) {
        const clonedData = cloneDeep(data)
        const targetArr = get(clonedData, path)
        const lastPart = path[path.length - 1]
        const obj = targetArr[idx]
        if (lastPart === 'questionnaires') {
          for (const question of obj.questions) {
            removeQuestionFiles(question)
          }
        } else if (lastPart === 'questions') {
          removeQuestionFiles(obj)
        }
        targetArr.splice(idx, 1)
        setData(clonedData)
        if (openItem === idx) {
          setOpenItem(-1)
        }
      },
      editTitle(path, idx, newTitle) {
        const clonedData = cloneDeep(data)
        const targetArr = get(clonedData, path)
        targetArr[idx].title = newTitle ? newTitle : 'Escribe un título'
        setData(clonedData)
      },
      down(path, idx) {
        if (idx < data.questionnaires.length - 1) {
          const clonedData = cloneDeep(data)
          const targetArr = get(clonedData, path)
          const aux = targetArr[idx]
          targetArr[idx] = targetArr[idx + 1]
          targetArr[idx + 1] = aux
          setData(clonedData)
        }
      },
      up(path, idx) {
        if (idx > 0) {
          const clonedData = cloneDeep(data)
          const targetArr = get(clonedData, path)
          const aux = targetArr[idx]
          targetArr[idx] = targetArr[idx - 1]
          targetArr[idx - 1] = aux
          setData(clonedData)
        }
      },
      editQuestion(questionPath, formData) {
        const clonedData = cloneDeep(data)
        const question = get(clonedData, questionPath)
        const id = question.id || uuidV4()
        question.id = id
        question.responseTime = formData.responseTime
        question.relatedResource = formData.relatedResource
        question.rightIndex = formData.rightIndex
        saveImage(question, 'img', formData)
        saveImage(question, 'answer0', formData)
        saveImage(question, 'answer1', formData)
        saveImage(question, 'answer2', formData)
        saveImage(question, 'answer3', formData)
        setData(clonedData)
      },
    }),
    [data, setData]
  )

  const value = React.useMemo(
    () => ({
      data,
      setData,
      model,
    }),
    [data, setData, model]
  )

  return <QuestionnariesContext.Provider value={value} {...props} />
}

function saveImage(question, fieldName, formData) {
  // if the user choose an image
  if (typeof formData[fieldName] === 'object') {
    const imgExt = getExtension(formData[fieldName].name)
    const questionFieldPath = getQuestionImg(question, fieldName, imgExt)
    // if there is already a file, remove it
    if (question[fieldName]) {
      if (fs.existsSync(questionFieldPath)) {
        const existentImage = getQuestionImg(
          question,
          fieldName,
          question[fieldName]
        )
        fs.unlinkSync(existentImage)
      }
    }
    fs.writeFileSync(questionFieldPath, '', 'utf-8')
    fs.createReadStream(formData[fieldName].path).pipe(
      fs.createWriteStream(questionFieldPath)
    )
    question[fieldName] = imgExt
  }
}

export function getQuestionImg(question, fieldName, imgExt) {
  return pathModule.join(
    questionnairesFolder,
    `${question.id}-${fieldName}.${imgExt}`
  )
}
