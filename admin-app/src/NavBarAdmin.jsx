import React from 'react'
import styled from 'styled-components'
import palette from './utils/palette'

export default ({
  handleDisplayMain,
  handleDisplayContents,
  handleDisplayQuestionnaires,
  handleDisplayCourses,
}) => {
  return (
    <NavBar>
      <Logo />
      <MainTitle>Panel de Administración</MainTitle>
      <TagContent>
        <MainTag onClick={handleDisplayMain}>Principal</MainTag>
        <CoursesTag onClick={handleDisplayCourses}>Cursos</CoursesTag>
        <ContentsTag onClick={handleDisplayContents}>Contenidos</ContentsTag>
        <QuestionnariesTag onClick={handleDisplayQuestionnaires}>
          Cuestionarios
        </QuestionnariesTag>
      </TagContent>
    </NavBar>
  )
}

const NavBar = styled.div`
  position: relative;
  display: flex;
  background-color: ${palette.white};
  padding: 0px;
  width: 100%;
  height: 50px;
  box-shadow: 3px 5px 5px 0 rgba(0, 0, 0, 0.13);
`
const Logo = styled.img.attrs({
  src: require('./assets/yupay-azul-01.png'),
  alt: 'Yupay',
})`
  width: 90px;
  height: 40px;
  margin: 5px 0px 5px 55px;
`
const MainTitle = styled.p`
  font-size: 18px;
  color: ${palette.tertiaryGray};
  margin: 15px 0px 5px 55px;
`
const TagContent = styled.div`
  position: absolute;
  right: 30px;
  bottom: 5px;
  display: flex;
  font-size: 18px;
  color: ${palette.tertiaryGray};
  width: 450px;
  justify-content: space-between;
`
const MainTag = styled.div`
  font-size: 18px;
  padding: 5px;
  cursor: pointer;
  color: ${palette.secundaryPurple};
  border: solid 2px ${palette.secundaryPurple};
  border-radius: 3px;
  &:hover {
    color: ${palette.white};
    background-color: ${palette.secundaryPurple};
    border: solid 2px ${palette.secundaryPurple};
  }
`

const CoursesTag = styled.div`
  font-size: 18px;
  padding: 5px;
  cursor: pointer;
  color: ${palette.primaryMandarin};
  border: solid 2px ${palette.primaryMandarin};
  border-radius: 3px;
  &:hover {
    color: ${palette.white};
    background-color: ${palette.primaryMandarin};
    border: solid 2px ${palette.primaryMandarin};
  }
`
const QuestionnariesTag = styled.div`
  font-size: 18px;
  padding: 5px;
  cursor: pointer;
  color: ${palette.secundaryRed};
  border: solid 2px ${palette.secundaryRed};
  border-radius: 3px;
  &:hover {
    color: ${palette.white};
    background-color: ${palette.secundaryRed};
    border: solid 2px ${palette.secundaryRed};
  }
`
const ContentsTag = styled.div`
  font-size: 18px;
  padding: 5px;
  cursor: pointer;
  color: ${palette.primaryBlue};
  border: solid 2px ${palette.primaryBlue};
  border-radius: 3px;
  &:hover {
    color: ${palette.white};
    background-color: ${palette.primaryBlue};
    border: solid 2px ${palette.primaryBlue};
  }
`
