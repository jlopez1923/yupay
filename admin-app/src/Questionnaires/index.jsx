/*QUESTIONS PANEL ADMIN (index)*/
import React, { useState, useCallback, useRef } from 'react'
import styled, { css } from 'styled-components'
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton'
import palette from '../utils/palette'

import { useQuestionnaires } from '../contexts/questionnaires'

import QuestionForm from './QuestionForm'

export default ({ show = true }) => {
  const { data } = useQuestionnaires()

  return (
    <List
      style={{ display: show ? 'block' : 'none' }}
      title="Cuestionarios"
      path={['questionnaires']}
      items={data.questionnaires}
      itemComponent={(questionnaires, questionnaireIdx) => (
        <List
          title="Preguntas"
          path={['questionnaires', questionnaireIdx, 'questions']}
          items={questionnaires.questions}
          itemComponent={(question, questionIdx) => (
            <QuestionForm
              path={[
                'questionnaires',
                questionnaireIdx,
                'questions',
                questionIdx,
              ]}
              question={question}
            />
          )}
        />
      )}
    />
  )
}
const List = ({ title, path, items, itemComponent, ...rest }) => {
  const [openItem, setItem] = useState(-1)
  const { model } = useQuestionnaires()

  const handlerOpenItem = useCallback(
    idx => {
      setItem(idx === openItem ? -1 : idx)
    },
    [openItem, setItem]
  )

  return (
    <MainContainer {...rest}>
      <Header>
        <MainTitle>{title}</MainTitle>
        <Tooltip title="Agregar" placement="right-start">
          <IconButton onClick={() => model.new(path)}>
            <Add />
          </IconButton>
        </Tooltip>
      </Header>
      <ListContainer>
        {items.map((item, idx) => (
          <Item key={item.id}>
            <TopicHeader>
              <Tooltip title={`Desplegar ${title}`} placement="bottom-end">
                <IconButton onClick={() => handlerOpenItem(item.id)}>
                  <Dropdown rotateArrow={openItem === item.id} />
                </IconButton>
              </Tooltip>
              <ItemTitle
                title={title}
                value={item.title}
                onChange={value => model.editTitle(path, idx, value)}
              />
              <Arrows>
                <Tooltip title="Mover hacia abajo" placement="left-start">
                  <IconButton onClick={() => model.down(path, idx)}>
                    <DownArrow />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Mover hacia arriba" placement="left-start">
                  <IconButton onClick={() => model.up(path, idx)}>
                    <UpArrow />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Eliminar" placement="right-start">
                  <IconButton onClick={() => model.remove(path, idx)}>
                    <Delete />
                  </IconButton>
                </Tooltip>
              </Arrows>
            </TopicHeader>
            {openItem === item.id && (
              <div style={{ padding: 40, paddingTop: 0, width: '100%' }}>
                {itemComponent(item, idx)}
              </div>
            )}
          </Item>
        ))}
      </ListContainer>
    </MainContainer>
  )
}

const MainContainer = styled.div`
  height: calc(100% - 50px);
  width: 100%;
`
const MainTitle = styled.div`
  font-size: 18px;
  color: ${palette.tertiaryGray};
  margin: 0px 20px 0px 5px;
`
const Add = styled.img.attrs({
  src: require('../assets/plus-red.svg'),
  alt: 'Agregar',
})`
  width: 40px;
  height: 40px;
  cursor: pointer;
`
const Header = styled.div`
  width: 100px;
  height: 90px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 50px;
`
const ListContainer = styled.div`
  width: 100%;
`
const Item = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding: 10px;
  border-bottom: 1px solid ${palette.tertiaryGray};
`
const TopicHeader = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
`
const Dropdown = styled.img.attrs({
  src: require('../assets/down-arrow-red.svg'),
  alt: 'Desplegar',
})`
  width: 18px;
  height: 18px;
  cursor: pointer;
  transition: all 0.4s ease;
  ${props =>
    props.rotateArrow &&
    css`
      transform: rotateZ(-180deg);
      transition: transform 0.3s;
    `}
`

const ItemTitle = ({ value, onChange, path }) => {
  const inputRef = useRef()
  const buttonRef = useRef()

  const [state, setState] = useState('show')
  const [inputValue, setInputValue] = useState(value)

  const handleItemClick = useCallback(() => {
    if (state === 'show') {
      setState('edit')
      setTimeout(() => {
        inputRef.current.focus()
      })
    }
  })

  const handleKeyPress = useCallback(
    event => {
      if (event.key === 'Enter') {
        save()
      }
    },
    [state, setState, inputValue]
  )

  const save = useCallback(() => {
    setState('show')
    onChange(inputValue)
  }, [state, setState, inputValue])

  const handleBlur = useCallback(
    ev => {
      if (ev.relatedTarget !== buttonRef.current) {
        save()
      } else {
        setInputValue(value)
      }
    },
    [state, setState, inputValue]
  )

  const cancel = useCallback(() => {
    setState('show')
  }, [state, setState, inputValue])

  return (
    <ItemTitleContainer onClick={handleItemClick}>
      {state === 'show' ? (
        value
      ) : (
        <>
          <NewItemInput
            ref={inputRef}
            value={inputValue}
            onBlur={handleBlur}
            onKeyPress={handleKeyPress}
            onChange={ev => setInputValue(ev.target.value)}
          />
          <Tooltip title="Cancelar Modificación" placement="bottom-end">
            <IconButton buttonRef={buttonRef} onClick={cancel}>
              <CancelIcon />
            </IconButton>
          </Tooltip>
        </>
      )}
    </ItemTitleContainer>
  )
}

const CancelIcon = styled.img.attrs({
  src: require('../assets/cancel.svg'),
  alt: 'Cancelar Modificación',
})`
  width: 20px;
  height: 20px;
  cursor: pointer;
`

const NewItemInput = styled.input`
  border: none;
  border-bottom: solid 1px ${palette.tertiaryGray};
  font-size: 18px;
  color: ${palette.tertiaryGray};
  outline: none;
`

const ItemTitleContainer = styled.div`
  font-size: 18px;
  color: ${palette.tertiaryGray};
  margin: 5px 20px 0px 5px;
`
const Arrows = styled.div`
  position: absolute;
  right: 30px;
  width: 150px;
`
const DownArrow = styled.img.attrs({
  src: require('../assets/down-red.svg'),
  alt: 'Subir',
})`
  width: 20px;
  height: 20px;
  cursor: pointer;
`
const UpArrow = styled.img.attrs({
  src: require('../assets/up-red.svg'),
  alt: 'Bajar',
})`
  width: 20px;
  height: 20px;
  cursor: pointer;
`
const Delete = styled.img.attrs({
  src: require('../assets/garbage-red.svg'),
  alt: 'Eliminar',
})`
  width: 20px;
  height: 20px;
  cursor: pointer;
`
