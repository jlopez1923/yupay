import React from 'react'
import styled from 'styled-components'
import { Formik, FastField, Field } from 'formik'
import { TextField } from 'formik-material-ui'
import { withStyles } from '@material-ui/core/styles'
import MenuItem from '@material-ui/core/MenuItem'
import grey from '@material-ui/core/colors/grey'
import palette from './../utils/palette'

import { useQuestionnaires, getQuestionImg } from '../contexts/questionnaires'
import { useContent } from '../contexts/contents'

import Select from './Select'

const answers = ['A', 'B', 'C', 'D']

const styles = () => ({
  cssLabel: {
    '&$cssFocused': {
      color: grey[400],
    },
  },
  cssFocused: {},
  cssUnderline: {
    '&:after': {
      borderBottomColor: grey[400],
    },
  },
  formControl: {
    minWidth: 50,
  },
  selectEmpty: {},
})

export default withStyles(styles)(function ResourceForm({
  classes,
  path,
  question,
}) {
  const { model } = useQuestionnaires()
  const { resources } = useContent()

  return (
    <Formik
      initialValues={question}
      validate={values => {
        const errors = {}
        if (!values.relatedResource) {
          errors.relatedResource =
            'Seleccione un recurso relacionado a esta pregunta'
        }
        if (!values.img) {
          errors.img = 'Sube la imagen de la pregunta'
        }
        if (!values.responseTime) {
          errors.responseTime = 'Sube la imagen de la pregunta'
        }
        for (let i = 0; i < 4; i++) {
          if (!values[`answer${i}`]) {
            errors[`answer${i}`] = 'Sube la imagen de una respuesta'
          }
        }
        return errors
      }}
      onSubmit={(values, actions) => {
        model.editQuestion(path, values)
        actions.setSubmitting(false)
        alert('La pregunta se guardó exitosamente :)')
      }}
      render={({ handleSubmit, setFieldValue, errors, touched, values }) => (
        <>
          <FormSection>
            <br />
            <ImageQuestionContent
              src={
                (values.img &&
                  (values.img.path
                    ? `file://${values.img.path}`
                    : `file://${getQuestionImg(
                        values,
                        'img',
                        question.img
                      )}`)) ||
                require('../assets/no-image.png')
              }
            />
            <UploadQuestionInput
              type="file"
              accept="image/*"
              id="questionInput"
              onChange={ev => setFieldValue('img', ev.target.files[0])}
            />
            <UploadQuestionLabel htmlFor="questionInput">
              Cargar Pregunta* <Size>(980px por 125px) </Size>
            </UploadQuestionLabel>
            {errors.question && touched.question && (
              <div style={{ color: 'red' }}>{errors.question}</div>
            )}
            {values.question && (
              <div>Se seleccionó el archivo: {values.question.name}</div>
            )}
            <br />

            <Selects>
              <Field
                name="relatedResource"
                label="Recurso Relacionado*"
                component={Select}
                formControl={{
                  variant: 'filled',
                  className: classes.formControl,
                }}
              >
                {resources.map(resource => (
                  <MenuItem key={resource.id} value={resource.id}>
                    {resource.title}
                  </MenuItem>
                ))}
              </Field>
              <FastField
                name="rightIndex"
                label="Respuesta correcta*"
                component={Select}
                formControl={{
                  variant: 'filled',
                  className: classes.formControl,
                }}
              >
                {answers.map((letter, idx) => (
                  <MenuItem key={idx} value={idx}>
                    {letter}
                  </MenuItem>
                ))}
              </FastField>
              <FastField
                name="responseTime"
                type="number"
                label="Tiempo de respuesta* (segundos)"
                style={{ width: '100%', marginBottom: 20 }}
                component={TextField}
              />
            </Selects>

            {answers.map((answer, idx) => (
              <FastField
                key={idx}
                name={`answer${idx}`}
                component={({ field, form, touched, errors }) => (
                  <>
                    <ImageAnswerContent
                      src={
                        (field.value &&
                          (field.value.path
                            ? `file://${field.value.path}`
                            : `file://${getQuestionImg(
                                values,
                                field.name,
                                question[field.name]
                              )}`)) ||
                        require('../assets/no-image-answer.png')
                      }
                    />
                    <AnswerContent>
                      <UploadAnswerLabel htmlFor={`answer${answer}Input`}>
                        Cargar Respuesta {answer}* <Size>(490px por 98px)</Size>
                      </UploadAnswerLabel>
                      <UploadAnswerInput
                        type="file"
                        accept="image/*"
                        id={`answer${answer}Input`}
                        onChange={ev => {
                          form.setFieldValue(`answer${idx}`, ev.target.files[0])
                        }}
                      />
                    </AnswerContent>
                    {errors &&
                      errors[`answer${idx}`] &&
                      touched &&
                      touched[`answer${idx}`] && (
                        <div style={{ color: 'red' }}>
                          {errors[`answer${idx}`]}
                        </div>
                      )}
                    {field.value && (
                      <div>Se seleccionó el archivo: {field.value.name}</div>
                    )}
                    <br />
                  </>
                )}
              />
            ))}
            <br />
            <Submit onClick={handleSubmit}>Guardar</Submit>
          </FormSection>
        </>
      )}
    />
  )
})

const FormSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
const Size = styled.div`
  font-size: 12px;
`
const Selects = styled.div`
  min-width: 400px;
  display: flex;
  flex-direction: column;
`
const UploadQuestionInput = styled.input`
  opacity: 0;
  position: absolute;
  pointer-events: none;
  width: 91px;
  height: 1px;
`
const UploadQuestionLabel = styled.label`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 430px;
  height: 50px;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  letter-spacing: 0.2em;
  transition: background-color 0.2s;
  background-color: ${palette.white};
  color: ${palette.secundaryRed};
  border: 2px solid ${palette.secundaryRed};
  &:hover {
    cursor: pointer;
    background: ${palette.secundaryRed};
    color: ${palette.white};
  }
`
const ImageQuestionContent = styled.img`
  width: auto;
  height: 125px;
  margin: 0px 10px 10px 10px;
`
const ImageAnswerContent = styled.img`
  width: auto;
  height: 100px;
  margin: 0px 10px 10px 10px;
`
const AnswerContent = styled.div`
  display: flex;
`
const UploadAnswerInput = styled.input`
  opacity: 0;
  position: absolute;
  pointer-events: none;
  width: 1px;
  height: 1px;
`
const UploadAnswerLabel = styled.label`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 430px;
  height: 50px;
  font-family: 'Quicksand', sans-serif;
  font-size: 16px;
  letter-spacing: 0.2em;
  transition: background-color 0.2s;
  background-color: ${palette.white};
  color: ${palette.secundaryRed};
  border: 2px solid ${palette.secundaryRed};
  &:hover {
    cursor: pointer;
    background: ${palette.secundaryRed};
    color: ${palette.white};
  }
`

const Submit = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 430px;
  height: 50px;
  font-family: 'Quicksand', sans-serif;
  font-weight: bold;
  font-size: 16px;
  letter-spacing: 0.2em;
  background: ${palette.secundaryRed};
  color: ${palette.white};
  transition: background-color 0.2s;
  outline: none;
  border: 2px solid ${palette.secundaryRed};
  &:hover {
    cursor: pointer;
    background-color: ${palette.white};
    color: ${palette.secundaryRed};
  }
`
