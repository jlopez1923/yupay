const express = require('express')
const fs = require('fs')
const path = require('path')
const cors = require('cors')
const ip = require('ip')
const bodyParser = require('body-parser')
const isDev = require('electron-is-dev')
const mime = require('mime')
const uuidV4 = require('uuid/v4')
const jwt = require('jsonwebtoken')
const expressJWT = require('express-jwt')
const { ipcMain } = require('electron')
const db = require('./db')
const logger = require('../logger')

const JWT_SECRET = 'Fsk23@4%^78JKlsdn0956adasd4dFGFsf32dg%Tg'

const electron = require('electron')
const app = electron.app

const userPath = app.getPath('userData')
const yupayPath = path.join(userPath, 'yupay-files')
if (!fs.existsSync(yupayPath)) {
  fs.mkdirSync(yupayPath)
}
const contentsFile = path.join(yupayPath, 'yupay-contents.json')
const contentsFolder = path.join(yupayPath, 'yupay-contents')
const yupayLogsFile = path.join(yupayPath, 'yupay-logs.txt')

const questionnairesFile = path.join(yupayPath, 'yupay-questionnaires.json')
const questionnairesFolder = path.join(yupayPath, 'yupay-questionnaires')

if (!fs.existsSync(contentsFolder)) {
  fs.mkdirSync(contentsFolder)
}

if (!fs.existsSync(questionnairesFolder)) {
  fs.mkdirSync(questionnairesFolder)
}

fs.writeFileSync(yupayLogsFile, '', 'utf8')

let contents
if (fs.existsSync(contentsFile)) {
  contents = JSON.parse(fs.readFileSync(contentsFile, 'utf-8'))
} else {
  contents = { topics: [] }
}

let questionnaires
if (fs.existsSync(questionnairesFile)) {
  questionnaires = JSON.parse(fs.readFileSync(questionnairesFile, 'utf-8'))
} else {
  questionnaires = { questionnaires: [] }
}

let savingContents
let saveScheduled = false

ipcMain.on('refresh-questionnaires', (event, arg) => {
  logger.log('refreshing questionnaires')
  questionnaires = JSON.parse(fs.readFileSync(questionnairesFile, 'utf-8'))
})

function saveContent() {
  if (!savingContents) {
    savingContents = new Promise(resolve => {
      fs.writeFile(contentsFile, JSON.stringify(contents), 'utf-8', err => {
        if (err) {
          reject()
          return
        }
        savingContents = undefined
        resolve()
      })
    })
  } else {
    if (!saveScheduled) {
      saveScheduled = true
      savingContents.then(() => {
        saveScheduled = false
        saveContent()
      })
    }
  }
}

const createContentServer = function() {
  const app = express()

  app.use(cors())

  app.use(bodyParser.json())

  app.use(express.static(path.join(__dirname, '../student-app-compiled')))

  app.get('/api', (req, res) => {
    res.json({
      msg: 'Yupay API running',
    })
  })

  app.get('/api/content', (req, res) => {
    res.json(contents)
  })

  app.get('/api/pdf/:fileName', (req, res) => {
    const filePath = path.join(contentsFolder, req.params.fileName)
    const stat = fs.statSync(filePath)
    const fileSize = stat.size
    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'application/pdf',
    }
    res.writeHead(200, head)
    fs.createReadStream(filePath).pipe(res)
  })

  app.get('/api/video/:fileName', (req, res) => {
    const filePath = path.join(contentsFolder, req.params.fileName)
    const stat = fs.statSync(filePath)
    const fileSize = stat.size
    const range = req.headers.range
    if (range) {
      const parts = range.replace(/bytes=/, '').split('-')
      const start = parseInt(parts[0], 10)
      const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1
      const chunksize = end - start + 1
      const file = fs.createReadStream(filePath, { start, end })
      const head = {
        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        'Accept-Ranges': 'bytes',
        'Content-Length': chunksize,
        'Content-Type': 'video/mp4',
      }
      res.writeHead(206, head)
      file.pipe(res)
    } else {
      const head = {
        'Content-Length': fileSize,
        'Content-Type': 'video/mp4',
      }
      res.writeHead(200, head)
      fs.createReadStream(filePath).pipe(res)
    }
  })

  app.post('/api/log', (req, res) => {
    const log = req.body
    // Se adjunta el log al archivo yupay-logs.txt
    fs.appendFileSync(yupayLogsFile, JSON.stringify(log) + '\n', 'utf8')
    res.json({ msg: 'success' })
  })

  app.post('/api/register', (req, res) => {
    const data = req.body
    const existentUser = db.find(
      'user',
      user => user.username === data.username
    )[0]
    if (existentUser) {
      res.json({ msg: 'user already exists' })
      return
    }
    const id = uuidV4()
    const { passwordHash, salt } = saltHashPassword(data.password)
    const user = {
      id,
      type: 'user',
      timestamp: Date.now(),
      username: data.username,
      fullName: data.fullName,
      grade: data.grade,
      gender: data.gender,
      age: data.age,
      avatar: data.avatar,
      learningStyle: data.learningStyle,
      intelligence: data.intelligence,
      passwordHash,
      salt,
      coins: 0,
    }
    const result = db.put(user)
    if (result) {
      const token = jwt.sign({ id: user.id }, JWT_SECRET)
      res.json({
        msg: 'success',
        user: { ...user, passwordHash: undefined, salt: undefined },
        token,
      })
    } else {
      res.status(401).json({ msg: 'unauthorized' })
    }
  })

  app.post('/api/login', (req, res) => {
    const data = req.body
    const user = db.find('user', user => user.username === data.username)[0]
    if (user) {
      const passwordHash = sha512(data.password, user.salt)
      if (passwordHash == user.passwordHash) {
        const token = jwt.sign({ id: user.id }, JWT_SECRET)
        res.json({
          msg: 'success',
          token,
          user: { ...user, passwordHash: undefined, salt: undefined },
        })
      } else {
        res.status(401).json({ msg: 'unauthorized' })
      }
    } else {
      res.status(401).json({ msg: 'unauthorized' })
    }
  })

  app.get('/api/thumbnails/:fileName', (req, res) => {
    const filePath = path.join(contentsFolder, req.params.fileName)
    const stat = fs.statSync(filePath)
    const fileSize = stat.size
    const mimeType = mime.getType(filePath)
    const head = {
      'Content-Length': fileSize,
      'Content-Type': mimeType,
    }
    res.writeHead(200, head)
    fs.createReadStream(filePath).pipe(res)
  })

  app.get('/api/view/:id', (req, res) => {
    const id = req.params.id
    if (!id) {
      res.status(404).json({ msg: 'not found' })
      return
    }
    for (const topic of contents.topics) {
      for (const subtopic of topic.subtopics) {
        for (const resource of subtopic.resources) {
          if (id === resource.id) {
            if (resource.views) {
              resource.views = resource.views + 1
            } else {
              resource.views = 1
            }
            saveContent()
            logger.log(resource.views)
            res.json({ msg: 'success' })
            return
          }
        }
      }
    }
    res.status(404).json({ msg: 'not found' })
  })

  // app.get('/api/like/:id', (req, res) => {
  //   for (const topic of contents.topics) {
  //     for (const subtopic of topic.subtopics) {
  //       for (const resource of subtopic.resources) {
  //         if (req.params.id === resource.id) {
  //           if (resource.like) {
  //             resource.like = resource.like + 1
  //           } else {
  //             resource.like = 1
  //           }
  //           saveContent()
  //           res.json({ msg: 'success' })
  //           return
  //         }
  //       }
  //     }
  //   }
  // })

  app.get('/api/question/:id/img/:fieldName', (req, res) => {
    const id = req.params.id
    const fieldName = req.params.fieldName
    if (!id) {
      res.status(404).json({ msg: 'not found' })
      return
    }
    for (const questionnaire of questionnaires.questionnaires) {
      for (const question of questionnaire.questions) {
        if (question.id === id) {
          const filePath = getQuestionImg(
            question,
            fieldName,
            question[fieldName]
          )
          const stat = fs.statSync(filePath)
          const fileSize = stat.size
          const mimeType = mime.getType(filePath)
          const head = {
            'Content-Length': fileSize,
            'Content-Type': mimeType,
          }
          res.writeHead(200, head)
          fs.createReadStream(filePath).pipe(res)
          return
        }
      }
    }
    res.status(404).json({ msg: 'not found' })
  })

  // --- PROTECTED ROUTES

  app.use(expressJWT({ secret: JWT_SECRET }))

  app.get('/api/questionnaire', (req, res) => {
    res.json(questionnaires)
  })

  app.get('/api/question/resource/:resourceId', (req, res) => {
    const resourceId = req.params.resourceId
    for (const questionnaire of questionnaires.questionnaires) {
      for (const question of questionnaire.questions) {
        if (question.relatedResource === resourceId) {
          res.json(questionnaire.questions.sort(() => Math.random() - 0.5))
          return
        }
      }
    }
    res.status(404).json({ msg: 'not found' })
  })

  app.use(function(err, req, res, next) {
    if (err) {
      logger.log('error', err)
    }
  })

  app.listen(8080, isDev ? 'localhost' : ip.address(), function() {
    logger.log('Listening on port 8080!')
  })
}

module.exports = createContentServer

function getQuestionImg(question, fieldName, imgExt) {
  return path.join(
    questionnairesFolder,
    `${question.id}-${fieldName}.${imgExt}`
  )
}

const crypto = require('crypto')

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
const genRandomString = function(length) {
  return crypto
    .randomBytes(Math.ceil(length / 2))
    .toString('hex') /** convert to hexadecimal format */
    .slice(0, length) /** return required number of characters */
}

/**
 * hash password with sha512.
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
const sha512 = function(password, salt) {
  const hash = crypto.createHmac('sha512', salt) /** Hashing algorithm sha512 */
  hash.update(password)
  const value = hash.digest('hex')
  return value
}

function saltHashPassword(password) {
  const salt = genRandomString(16) /** Gives us salt of length 16 */
  const passwordHash = sha512(password, salt)
  return { passwordHash, salt }
}
