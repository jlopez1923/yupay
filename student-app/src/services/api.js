export const isProduction = process.env.NODE_ENV === 'production'

export const serverURL =
  process.env.NODE_ENV === 'production' ? '' : 'http://localhost:8080'

const jsonHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
}

const localStorageTokenKey = 'app-token'

const state = {
  token: localStorage.getItem(localStorageTokenKey),
}

function setToken(token) {
  state.token = token
  localStorage.setItem(localStorageTokenKey, token)
}

function handleError(response) {
  if (!response.ok) {
    throw Error(response.status)
  }
  return response
}

export const get = path =>
  fetch(`${serverURL}${path}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${state.token}`,
      ...jsonHeaders,
    },
  })
    .then(handleError)
    .then(r => r.json())

export const post = (path, data) =>
  fetch(`${serverURL}${path}`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${state.token}`,
      ...jsonHeaders,
    },
    body: JSON.stringify(data),
  })
    .then(handleError)
    .then(r => r.json())

export const remove = path =>
  fetch(`${serverURL}${path}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${state.token}`,
      ...jsonHeaders,
    },
  })
    .then(handleError)
    .then(r => r.json())

export const isAuth = () => !!state.token

export const login = data =>
  post('/api/login', data).then(res => {
    setToken(res.token)
    return res
  })

export const register = data =>
  post('/api/register', data).then(res => {
    setToken(res.token)
    return res
  })

export const signOut = () => {
  setToken('')
}

export const getVideo = resource =>
  `${serverURL}/api/video/${resource.id}.${resource.src}`

export const getPDF = resource =>
  `${serverURL}/api/pdf/${resource.id}.${resource.src}`

export const getThumbnails = resource =>
  `${serverURL}/api/thumbnails/${resource.id}.${resource.img}`

export const getContent = () =>
  fetch(`${serverURL}/api/content`).then(r => r.json())

export const getQuestions = resourceId =>
  fetch(`${serverURL}/api/question/resource/${resourceId}`, {
    headers: {
      Authorization: `Bearer ${state.token}`,
    },
  }).then(r => r.json())

export const getQuestionImg = (question, fieldName) =>
  `${serverURL}/api/question/${question.id}/img/${fieldName}`

export const view = id => fetch(`${serverURL}/api/view/${id}`)

export const log = logObject => {
  fetch(`${serverURL}/api/log`, {
    method: 'POST',
    headers: jsonHeaders,
    body: JSON.stringify(logObject),
  })
    .then(handleError)
    .then(r => r.json())
}
