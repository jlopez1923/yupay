export default {
  white: '#ffffff',
  primaryBlue: '#52C9EC',
  secundaryBlue: '#2684FF',
  primaryGray: '#e5e5e5',
  secundaryGray: '#999999',
  tertiaryGray: '#a79fa5',
  primaryYellow: '#FBED21',
  primaryPurple: '#7d5fff',
  secundaryPurple: '#a865ff',
  primaryPink: '#ff65bc',
  primaryRed: '#ff3838',
  secundaryRed: '#ff4d4d',
  primaryGreen: '#32ff7e',
  secundaryGreen: '#3ae374',
  primaryMandarin: '#ffaf40',

  black: '#000',
  secundaryBlack: '#4b4b4b',

  primaryBoxShadow: '3px 5px 5px 0 rgba(0, 0, 0, 0.13)',
  secundaryBoxShadow: '0px 10px 28px 4px #d2d2d2',
}
//${palette.}
