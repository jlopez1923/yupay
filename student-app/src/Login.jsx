import React, { useContext } from 'react'
import { withRouter } from 'react-router-dom'
import { useSession } from './contexts/session'
import { logSesionIniciada } from './services/log'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { withStyles, createMuiTheme } from '@material-ui/core/styles'
import grey from '@material-ui/core/colors/grey'
import { Formik, Field } from 'formik'
import { TextField } from 'formik-material-ui'
import palette from './utils/palette'

import './App.css'

import * as api from './services/api'

const styles = () => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing.unit,
  },
  cssLabel: {
    '&$cssFocused': {
      color: grey[400],
    },
  },
  cssFocused: {},
  cssUnderline: {
    '&:after': {
      borderBottomColor: grey[400],
    },
  },
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: grey[400],
    },
  },
  notchedOutline: {},
})

const theme = createMuiTheme({
  typography: { useNextVariants: true },
})

export default withStyles(styles)(
  withRouter(({ history }) => {
    const { setSession } = useSession()

    const start = data => {
      setSession(data)
      logSesionIniciada(data.username)
      history.push('/buscador')
    }

    return (
      <LoginContent>
        <RightCloud />
        <Card>
          <LogoContent>
            <LogoImg />
          </LogoContent>
          <InputContent>
            <Formik
              initialValues={{ username: '', password: '' }}
              validate={values => {
                const errors = {}
                if (!values.username) {
                  errors.username = 'Asegúrate de escribir tu nombre'
                }
                if (!values.password) {
                  errors.password = 'Escribe una contraseña'
                }

                return errors
              }}
              onSubmit={(values, actions) => {
                api
                  .login(values)
                  .then(res => {
                    start(res.user)
                  })
                  .catch(() => {
                    alert('Error de autenticación')
                    actions.setSubmitting(false)
                  })
              }}
              render={({ submitForm }) => (
                <div>
                  <StyledField
                    name="username"
                    type="text"
                    label="Escribe tu nombre"
                    component={TextField}
                  />
                  <StyledField
                    name="password"
                    type="password"
                    label="Contraseña"
                    component={TextField}
                  />
                  <LoginBtn onClick={submitForm}>Ingresar</LoginBtn>
                  <StyedLink to="/Register">Registrarse</StyedLink>
                  {/* <LoginBtn onClick={submitForm}>Entrar sin registrarse</LoginBtn> */}
                </div>
              )}
            />
          </InputContent>
        </Card>
        <LeftCloud />
      </LoginContent>
    )
  })
)

const LoginContent = styled.div`
  margin-top: 5%;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`
const RightCloud = styled.img.attrs({
  src: require('./assetsStudent/nube_derecha.png'),
  alt: 'Nube derecha',
})`
  position: absolute;
  top: 0px;
  right: 0px;
  width: 300px;
`
const Card = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 450px;
  width: 420px;
  background-color: ${palette.white};
  box-shadow: ${palette.secundaryBoxShadow};
`
const LogoContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  top: -90px;
  width: 154px;
  height: 154px;
  border-radius: 50%;
  background: ${palette.primaryBlue};
`
const LogoImg = styled.img.attrs({
  src: require('./assetsStudent/Lobo-yupay-01.svg'),
  alt: 'Logo',
})`
  width: 100px;
  height: 100px;
`
const InputContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 40%;
  padding-left: 60px;
  width: 80%;
`
const StyledField = styled(Field)`
  width: 295px;
`
const LoginBtn = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 295px;
  height: 50px;
  margin-top: 30px;
  font-family: 'Quicksand', sans-serif;
  font-weight: bold;
  font-size: 16px;
  letter-spacing: 0.2em;
  background-color: ${palette.primaryYellow};
  border: none;
`
const StyedLink = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 295px;
  height: 50px;
  color: ${palette.black};
  margin-top: 30px;
  font-family: 'Quicksand', sans-serif;
  font-weight: bold;
  font-size: 16px;
  letter-spacing: 0.2em;
  background-color: ${palette.primaryYellow};
  text-decoration: none;
`
const LeftCloud = styled.img.attrs({
  src: require('./assetsStudent/nube_izquierda.png'),
  alt: 'Nube baja',
})`
  position: absolute;
  left: 0px;
  bottom: -70px;
  width: 600px;
  height: 400px;
  z-index: -1;
`
