import React from 'react'
import styled, { css } from 'styled-components'
import Tooltip from '@material-ui/core/Tooltip'
import palette from '../../utils/palette'
import { Link } from 'react-router-dom'

export default ({
  activity,
  relatedResource,
  onPrevious,
  onNext,
  onEnd,
  disablePrevius,
  disableNext,
  showNext,
  showEnd,
}) => {
  return (
    <Bottons>
      {activity === 'training' && (
        <>
          <Tooltip title="Regresa a la pregunta anterior" placement="top">
            <Previous onClick={onPrevious} disabled={disablePrevius}>
              Anterior
            </Previous>
          </Tooltip>
          <Tooltip title="Ayuda" placement="top">
            <Link
              to={`/buscador/learning?option=resource&id=${relatedResource}`}
            >
              <HelpIcon />
            </Link>
          </Tooltip>
        </>
      )}
      {activity === 'competition' && (
        <Tooltip title="Haz click para saltar esta pregunta" placement="top">
          <SkipBtn>Saltar pregunta</SkipBtn>
        </Tooltip>
      )}
      {activity === 'competition' && <ManyQuestions>2/10</ManyQuestions>}
      {showNext && (
        <Tooltip
          title="Haz click aquí para continuar con la siguiente pregunta"
          placement="top"
        >
          <NextBtn onClick={onNext} disabled={disableNext}>
            Siguiente
          </NextBtn>
        </Tooltip>
      )}
      {showEnd && (
        <Tooltip title="Haz click aquí para terminar" placement="top">
          <NextBtn onClick={onEnd}>Terminar</NextBtn>
        </Tooltip>
      )}
    </Bottons>
  )
}

const Bottons = styled.div`
  display: flex;
  width: 100%;
  height: 100px;
  justify-content: space-around;
`
const SkipBtn = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 30px;
  font-weight: bold;
  font-size: 16px;
  padding: 0px 10px;
  letter-spacing: 0.2em;
  background: ${palette.white};
  color: ${palette.black};
  border: 2px solid ${palette.primaryYellow};
  &:hover {
    cursor: pointer;
    background: ${palette.primaryYellow};
    border: 2px solid ${palette.primaryYellow};
    color: ${palette.black};
  }
`
const ManyQuestions = styled.h4`
  display: flex;
  width: 50px;
  height: 30px;
`
const NextBtn = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 30px;
  font-weight: bold;
  font-size: 16px;
  padding: 0px 10px;
  letter-spacing: 0.2em;
  transition: background-color 0.2s;
  background-color: ${palette.white};
  color: ${props =>
    props.disabled ? palette.primaryGrey : palette.primaryBlue};
  border: 2px solid
    ${props => (props.disabled ? palette.primaryGrey : palette.primaryBlue)};
  &:hover {
    ${props =>
      !props.disabled &&
      css`
        cursor: pointer;
        background: ${palette.primaryBlue};
        border: 2px solid ${palette.primaryBlue};
        color: ${palette.black};
      `}
  }
`
const HelpIcon = styled.img.attrs({
  src: require('../assetsDashboard/help.svg'),
  alt: 'Ayuda',
})`
  height: 30px;
  widht: 30px;
  &:hover {
    cursor: pointer;
  }
`
const Previous = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 30px;
  font-weight: bold;
  font-size: 16px;
  padding: 0px 10px;
  letter-spacing: 0.2em;
  background-color: ${palette.white};
  color: ${props =>
    props.disabled ? palette.primaryGrey : palette.primaryPink};
  transition: background-color 0.2s;
  border: 2px solid
    ${props => (props.disabled ? palette.primaryGrey : palette.primaryPink)};
  &:hover {
    ${props =>
      !props.disabled &&
      css`
        cursor: pointer;
        border: 2px solid ${palette.primaryPink};
        background-color: ${palette.primaryPink};
        color: ${palette.black};
      `}
  }
`
