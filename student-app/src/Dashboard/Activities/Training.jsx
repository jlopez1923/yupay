import React from 'react'
import styled from 'styled-components'
import NavBar from '../NavBar/index'
import palette from '../../utils/palette'
import Activity from './Activity'

export default () => {
  return (
    <ActivityContent>
      <NavBar />
      <Activity activity="training" idSubtopic="identificador del tema" />
    </ActivityContent>
  )
}

const ActivityContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${palette.white};
  width: 100%;
  height: 100%;
`
