import React from 'react'
import styled, { css } from 'styled-components'
import { withRouter } from 'react-router-dom'
import palette from '../../utils/palette'
import Right from '../components/Right'
import Wrong from '../components/Wrong'
import Status from '../components/Status'
import ButtonActions from './ButtonsActions'

import { useSession } from '../../contexts/session'
import { getQuestionImg, getQuestions } from '../../services/api'

export default withRouter(({ activity, location }) => {
  const session = useSession()

  const [state, setState] = React.useState('answering')
  const [questions, setQuestions] = React.useState([])
  const [activeIndex, setActiveIndex] = React.useState(-1)
  const [activeAnswers, setAnswers] = React.useState([])

  const [rightAnswers, setRightAnswers] = React.useState(0)
  const [coinsEarned, setCoinsEarned] = React.useState(0)

  React.useEffect(() => {
    const params = new URLSearchParams(location.search)
    const resourceId = params.get('id')

    getQuestions(resourceId).then(res => {
      if (res.length > 0) {
        setQuestions(res)
        setActiveIndex(0)
        setAnswers(res.map(() => -1))
      }
    })
  }, [])

  const [feedbackState, setFeedbackState] = React.useState('hidden')

  const setAnswer = React.useCallback(
    idx => {
      const tempArr = activeAnswers.slice(0)
      tempArr[activeIndex] = idx
      setAnswers(tempArr)
      setFeedbackState('visible')
      setTimeout(() => {
        setFeedbackState('hidden')
      }, 1500)
    },
    [activeAnswers, activeIndex]
  )

  const onPrevious = React.useCallback(() => {
    if (activeIndex - 1 >= 0) {
      setActiveIndex(activeIndex - 1)
    }
  }, [activeIndex])

  const onNext = React.useCallback(() => {
    if (activeIndex < questions.length - 1) {
      setActiveIndex(activeIndex + 1)
    }
  }, [activeIndex])

  const onEnd = React.useCallback(() => {
    let rightAnswersCount = 0
    for (let index = 0; index < activeAnswers.length; index++) {
      const activeAnswer = activeAnswers[index]
      if (activeAnswer === questions[index].rightIndex) {
        rightAnswersCount++
      }
    }
    setRightAnswers(rightAnswersCount)
    let coins = rightAnswersCount * 50
    setCoinsEarned(coins)
    session.addCoins(coins)
    setState('score')
  }, [activeAnswers, questions])

  const question = questions[activeIndex]
  const activeAnswerIndex = activeAnswers[activeIndex]

  return (
    <Container>
      {questions.length === 0 && <h1>No hay preguntas</h1>}
      {activeIndex !== -1 && // because the questions are loaded asynchronously
        (state === 'answering' ? (
          <>
            <QuestionContent>
              {activity === 'competition' && <Timer>2:50 min</Timer>}
              <Question src={getQuestionImg(question, 'img')} />
              {activeAnswerIndex !== -1 &&
                feedbackState === 'visible' &&
                (activeAnswerIndex === question.rightIndex ? (
                  <Right />
                ) : (
                  <Wrong />
                ))}
            </QuestionContent>
            <AnswerContent>
              <TopRow>
                <Answer
                  type="A"
                  src={getQuestionImg(question, 'answer0')}
                  active={activeAnswerIndex === 0}
                  onClick={() => setAnswer(0)}
                />
                <Answer
                  type="B"
                  src={getQuestionImg(question, 'answer1')}
                  active={activeAnswerIndex === 1}
                  onClick={() => setAnswer(1)}
                />
              </TopRow>
              <BottomRow>
                <Answer
                  type="C"
                  src={getQuestionImg(question, 'answer2')}
                  active={activeAnswerIndex === 2}
                  onClick={() => setAnswer(2)}
                />
                <Answer
                  type="D"
                  src={getQuestionImg(question, 'answer3')}
                  active={activeAnswerIndex === 3}
                  onClick={() => setAnswer(3)}
                />
              </BottomRow>
            </AnswerContent>
            <ButtonActions
              activity={activity}
              relatedResource={question.relatedResource}
              onPrevious={onPrevious}
              onNext={onNext}
              onEnd={onEnd}
              disablePrevius={activeIndex === 0}
              disableNext={
                activity === 'competition' ? activeAnswerIndex !== -1 : false
              }
              showNext={activeIndex !== questions.length - 1}
              showEnd={activeIndex === questions.length - 1}
            />
          </>
        ) : (
          <Status rightAnswers={rightAnswers} coins={coinsEarned} />
        ))}
    </Container>
  )
})

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background-color: ${palette.white};
  width: 100%;
  height: 100%;
`
const QuestionContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 40%;
`
const Timer = styled.div`
  position: absolute;
  top: 165px;
  left: 120px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 130px;
  height: 130px;
  font-size: 30px;
  color: ${palette.white};
  background: ${palette.primaryBlue};
  border-radius: 50%;
`
const Question = styled.img`
  width: auto;
  height: 125px;
`

const AnswerContent = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 10px;
`
const TopRow = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  justify-content: space-around;
`
const BottomRow = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  justify-content: space-around;
`

const answerColor = {
  A: palette.primaryMandarin,
  B: palette.primaryPink,
  C: palette.primaryPurple,
  D: palette.primaryGreen,
}

const Answer = ({ type, active, src, ...rest }) => (
  <AnswerContainer type={type} active={active} {...rest}>
    <AnswerImg src={src} />
  </AnswerContainer>
)

const AnswerContainer = styled.div`
  display: flex;
  justify-content: center;
  padding: 20px;
  width: 490px;
  height: 90px;
  border: 4px solid ${palette.primaryGray};
  overflow: hidden;
  cursor: pointer;
  ${props =>
    props.active &&
    css`
      border: 4px solid ${props => answerColor[props.type]};
    `}
`

const AnswerImg = styled.img`
  width: auto;
  height: 90px;
`
